package com.lewitt.pietrogirardi.lewitt.presentation.presenter;

import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.lewitt.pietrogirardi.lewitt.domain.entities.Shot;
import com.lewitt.pietrogirardi.lewitt.domain.interactor.Callback;
import com.lewitt.pietrogirardi.lewitt.domain.interactor.IGetImagesUseCase;
import com.lewitt.pietrogirardi.lewitt.presentation.presenter.views.IShotView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by pietrogirardi on 09/07/16.
 */
public class ShotPresenterImpl extends RecyclerView.OnScrollListener implements IShotPresenter {

    @Inject
    IGetImagesUseCase mGetImagesUseCase;

    @Nullable
    IShotView mShotView;

    private boolean mIsLoadingData;
    private int firstVisibleItem, visibleItemCount, totalItemCount, previousTotal = 0, visibleThreshold = 2;
    private int current_page = 1;

    private List<Shot> shotList;


    @Inject
    public ShotPresenterImpl() {
    }

    @Override
    public void loadData(int page) {
        showLoading();

        mGetImagesUseCase.execute(this.current_page, new Callback<List<Shot>>() {
            @Override
            public void onSuccess(List<Shot> shots) {
                showImages(shots);
            }

            @Override
            public void onException(Exception e) {
                showError(e.getMessage());
            }

            @Override
            public void onPostExecute() {
                hideLoading();
            }
        });


    }

    @Override
    public void attachView(IShotView shotView) {
        this.mShotView = shotView;
        if (mIsLoadingData) {
            this.mShotView.showLoading();
        }
    }

    @Override
    public void clickedOnShot(Shot shot) {

    }

    @Override
    public void detachView() {

    }


    public void showLoading() {
        mIsLoadingData = true;

        if (mShotView != null)
            mShotView.showLoading();
    }

    public void hideLoading() {
        mIsLoadingData = false;

        if (mShotView != null)
            mShotView.hideLoading();
    }

    public void showImages(List<Shot> shots) {
        if (mShotView != null){
            if(shotList == null){
                shotList = new ArrayList<Shot>();
            }
            shotList.addAll(shots);
            mShotView.showItems(shotList);
        }
    }

    public void showError(String error) {
        if (mShotView != null)
            mShotView.showError(error);
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();

        totalItemCount = layoutManager.getItemCount();
        visibleItemCount = layoutManager.getChildCount();

        if (mIsLoadingData) {
            if (totalItemCount > previousTotal) {
                previousTotal = totalItemCount;
            }
        }

        if(layoutManager instanceof GridLayoutManager){
            firstVisibleItem = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
        }else{
            firstVisibleItem = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
        }

        if (!mIsLoadingData && (firstVisibleItem > totalItemCount-visibleThreshold)) {
            // End has been reached
            current_page++;
            if(current_page>=2){ //page one loads from shot fragment
                loadData(current_page);
                mIsLoadingData = true;
            }
        }
    }
}
