package com.lewitt.pietrogirardi.lewitt.presentation.screens;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lewitt.pietrogirardi.lewitt.AndroidApplication;
import com.lewitt.pietrogirardi.lewitt.presentation.di.ApplicationComponent;

/**
 * Created by pietrogirardi on 10/07/16.
 */
public class BaseFragment extends Fragment {

    protected ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApplicationComponent =  ((AndroidApplication) getActivity().getApplication()).getApplicationComponent();
        mApplicationComponent.inject(this);
    }
}
