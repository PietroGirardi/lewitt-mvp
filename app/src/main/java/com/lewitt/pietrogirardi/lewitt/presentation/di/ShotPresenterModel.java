package com.lewitt.pietrogirardi.lewitt.presentation.di;

import android.app.Application;

import com.lewitt.pietrogirardi.lewitt.domain.interactor.GetImagesUseCaseImpl;
import com.lewitt.pietrogirardi.lewitt.domain.interactor.IGetImagesUseCase;
import com.lewitt.pietrogirardi.lewitt.domain.repository.ShotRepository;
import com.lewitt.pietrogirardi.lewitt.presentation.interactor.AndroidUiThreadExecutor;
import com.lewitt.pietrogirardi.lewitt.presentation.presenter.IShotPresenter;
import com.lewitt.pietrogirardi.lewitt.presentation.presenter.ShotPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by p.girardi on 7/8/2016.
 */

@Module
public class ShotPresenterModel {

    Application application;

    public ShotPresenterModel(Application application) {
        this.application = application;
    }

    @Provides public IShotPresenter provideShotPresenter(ShotPresenterImpl shotPresenter) {
        return shotPresenter;
    }

    @Provides public IGetImagesUseCase provideGetImagesUseCase(AndroidUiThreadExecutor executor,
                                                                 ShotRepository repository) {
        return new GetImagesUseCaseImpl(executor, repository);
    }

}
