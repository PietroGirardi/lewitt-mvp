package com.lewitt.pietrogirardi.lewitt.presentation.di;


import android.content.Context;

import com.lewitt.pietrogirardi.lewitt.data.di.ShotDataModel;
import com.lewitt.pietrogirardi.lewitt.presentation.screens.BaseFragment;
import com.lewitt.pietrogirardi.lewitt.presentation.screens.ShotDetailFragmentView;
import com.lewitt.pietrogirardi.lewitt.presentation.screens.ShotFragmentView;

import dagger.Component;

@Component(modules = {
        ShotPresenterModel.class,
        ShotDataModel.class
})
public interface ApplicationComponent {
    void inject(BaseFragment baseFragment);
    void inject(ShotFragmentView shotFragmentView);
    void inject(ShotDetailFragmentView shotDetailFragmentView);

    //Context provideContext();
}
