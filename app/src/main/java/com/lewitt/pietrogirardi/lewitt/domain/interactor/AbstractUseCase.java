package com.lewitt.pietrogirardi.lewitt.domain.interactor;

public abstract class AbstractUseCase {
    protected IUiThreadExecutor mUiThreadExecutor;
    protected Thread mProcess;

    public AbstractUseCase(IUiThreadExecutor mUiThreadExecutor) {
        this.mUiThreadExecutor = mUiThreadExecutor;
    }

    protected void executeAsync(Runnable runnable) {
        mProcess = new Thread(runnable);
        mProcess.start();
    }
}
