package com.lewitt.pietrogirardi.lewitt.presentation.presenter;

import com.lewitt.pietrogirardi.lewitt.domain.entities.Shot;
import com.lewitt.pietrogirardi.lewitt.presentation.presenter.views.IShotView;

/**
 * Created by pietrogirardi on 09/07/16.
 */
public interface IShotPresenter extends IPresenter {
    void loadData(int page);
    void attachView(IShotView imageView);
    void clickedOnShot(Shot shot);
    void detachView();
}
