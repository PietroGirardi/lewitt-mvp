package com.lewitt.pietrogirardi.lewitt.domain.interactor;

import android.util.Log;

import com.lewitt.pietrogirardi.lewitt.domain.entities.Shot;
import com.lewitt.pietrogirardi.lewitt.domain.repository.ShotRepository;

import java.util.List;

/**
 * Created by pietrogirardi on 09/07/16.
 */
public class GetImagesUseCaseImpl extends AbstractUseCase implements IGetImagesUseCase {

    private ShotRepository mShotRepository;
    private Callback<List<Shot>> mCallback;
    private int mPage;

    public GetImagesUseCaseImpl(IUiThreadExecutor mUiThreadExecutor, ShotRepository shotRepository) {
        super(mUiThreadExecutor);
        this.mShotRepository = shotRepository;
    }

    @Override
    public void execute(int page, Callback<List<Shot>> callback) {
        this.mPage = page;
        this.mCallback = callback;
        this.mCallback.setUiThreadExecutor(mUiThreadExecutor);
        executeAsync(this);
        Log.i("test", "execute");
    }

    @Override
    public void run() {
        try {
            List<Shot> shots = mShotRepository.listShot(this.mPage);
            mCallback.dispatchResult(shots);
        } catch (Exception e) {
            mCallback.dispatchException(e);
        }
    }

}
