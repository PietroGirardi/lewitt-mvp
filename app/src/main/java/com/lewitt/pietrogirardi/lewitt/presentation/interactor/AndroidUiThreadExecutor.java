package com.lewitt.pietrogirardi.lewitt.presentation.interactor;

import android.os.Handler;
import android.os.Looper;

import com.lewitt.pietrogirardi.lewitt.domain.interactor.IUiThreadExecutor;

import javax.inject.Inject;

public class AndroidUiThreadExecutor implements IUiThreadExecutor {
    Handler handler;

    @Inject
    public AndroidUiThreadExecutor() {
        this.handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void execute(Runnable runnable) {
        handler.post(runnable);
    }
}
