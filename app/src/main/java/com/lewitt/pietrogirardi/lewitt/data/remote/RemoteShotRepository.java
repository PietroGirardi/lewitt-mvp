package com.lewitt.pietrogirardi.lewitt.data.remote;

import android.util.Log;

import com.lewitt.pietrogirardi.lewitt.R;
import com.lewitt.pietrogirardi.lewitt.data.api.IShotAPI;
import com.lewitt.pietrogirardi.lewitt.data.exception.RemoteShotRepositoryException;
import com.lewitt.pietrogirardi.lewitt.domain.entities.Shot;
import com.lewitt.pietrogirardi.lewitt.domain.repository.ShotRepository;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by pietrogirardi on 10/07/16.
 */
public class RemoteShotRepository implements ShotRepository{
    private static final String TOKEN = "6288d1baba6d36c25f2b62037e644f450d6e8d2b8c68d064bae2adeed090d6d3";

    @Inject IShotAPI mShotApi;


    @Inject
    public RemoteShotRepository() {
    }

    @Override
    public List<Shot> listShot(int page) throws Exception {
        Call<List<Shot>> call = mShotApi.getListShot(TOKEN, page);

        Response<List<Shot>> response = call.execute();

        if(response.isSuccessful()){
            return response.body();
        }else{
            throw new RemoteShotRepositoryException(R.string.remote_shot_repository_exception+response.message());
        }
    }

    @Override
    public Shot retrieveShotDetail(Shot Shot) throws Exception {
        return null;
    }
}
