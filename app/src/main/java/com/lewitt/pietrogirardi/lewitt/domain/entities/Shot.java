
package com.lewitt.pietrogirardi.lewitt.domain.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Shot implements Comparable<Shot>, Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("width")
    @Expose
    private int width;
    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("images")
    @Expose
    private Images images;
    @SerializedName("views_count")
    @Expose
    private int viewsCount;
    @SerializedName("likes_count")
    @Expose
    private int likesCount;
    @SerializedName("comments_count")
    @Expose
    private int commentsCount;
    @SerializedName("attachments_count")
    @Expose
    private int attachmentsCount;
    @SerializedName("rebounds_count")
    @Expose
    private int reboundsCount;
    @SerializedName("buckets_count")
    @Expose
    private int bucketsCount;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("html_url")
    @Expose
    private String htmlUrl;
    @SerializedName("attachments_url")
    @Expose
    private String attachmentsUrl;
    @SerializedName("buckets_url")
    @Expose
    private String bucketsUrl;
    @SerializedName("comments_url")
    @Expose
    private String commentsUrl;
    @SerializedName("likes_url")
    @Expose
    private String likesUrl;
    @SerializedName("projects_url")
    @Expose
    private String projectsUrl;
    @SerializedName("rebounds_url")
    @Expose
    private String reboundsUrl;
    @SerializedName("rebound_source_url")
    @Expose
    private String reboundSourceUrl;
    @SerializedName("animated")
    @Expose
    private boolean animated;
    @SerializedName("tags")
    @Expose
    private List<String> tags = new ArrayList<String>();
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("team")
    @Expose
    private Object team;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The width
     */
    public int getWidth() {
        return width;
    }

    /**
     * 
     * @param width
     *     The width
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * 
     * @return
     *     The height
     */
    public int getHeight() {
        return height;
    }

    /**
     * 
     * @param height
     *     The height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * 
     * @return
     *     The images
     */
    public Images getImages() {
        return images;
    }

    /**
     * 
     * @param images
     *     The images
     */
    public void setImages(Images images) {
        this.images = images;
    }

    /**
     * 
     * @return
     *     The viewsCount
     */
    public int getViewsCount() {
        return viewsCount;
    }

    /**
     * 
     * @param viewsCount
     *     The views_count
     */
    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    /**
     * 
     * @return
     *     The likesCount
     */
    public int getLikesCount() {
        return likesCount;
    }

    /**
     * 
     * @param likesCount
     *     The likes_count
     */
    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    /**
     * 
     * @return
     *     The commentsCount
     */
    public int getCommentsCount() {
        return commentsCount;
    }

    /**
     * 
     * @param commentsCount
     *     The comments_count
     */
    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    /**
     * 
     * @return
     *     The attachmentsCount
     */
    public int getAttachmentsCount() {
        return attachmentsCount;
    }

    /**
     * 
     * @param attachmentsCount
     *     The attachments_count
     */
    public void setAttachmentsCount(int attachmentsCount) {
        this.attachmentsCount = attachmentsCount;
    }

    /**
     * 
     * @return
     *     The reboundsCount
     */
    public int getReboundsCount() {
        return reboundsCount;
    }

    /**
     * 
     * @param reboundsCount
     *     The rebounds_count
     */
    public void setReboundsCount(int reboundsCount) {
        this.reboundsCount = reboundsCount;
    }

    /**
     * 
     * @return
     *     The bucketsCount
     */
    public int getBucketsCount() {
        return bucketsCount;
    }

    /**
     * 
     * @param bucketsCount
     *     The buckets_count
     */
    public void setBucketsCount(int bucketsCount) {
        this.bucketsCount = bucketsCount;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The htmlUrl
     */
    public String getHtmlUrl() {
        return htmlUrl;
    }

    /**
     * 
     * @param htmlUrl
     *     The html_url
     */
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    /**
     * 
     * @return
     *     The attachmentsUrl
     */
    public String getAttachmentsUrl() {
        return attachmentsUrl;
    }

    /**
     * 
     * @param attachmentsUrl
     *     The attachments_url
     */
    public void setAttachmentsUrl(String attachmentsUrl) {
        this.attachmentsUrl = attachmentsUrl;
    }

    /**
     * 
     * @return
     *     The bucketsUrl
     */
    public String getBucketsUrl() {
        return bucketsUrl;
    }

    /**
     * 
     * @param bucketsUrl
     *     The buckets_url
     */
    public void setBucketsUrl(String bucketsUrl) {
        this.bucketsUrl = bucketsUrl;
    }

    /**
     * 
     * @return
     *     The commentsUrl
     */
    public String getCommentsUrl() {
        return commentsUrl;
    }

    /**
     * 
     * @param commentsUrl
     *     The comments_url
     */
    public void setCommentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
    }

    /**
     * 
     * @return
     *     The likesUrl
     */
    public String getLikesUrl() {
        return likesUrl;
    }

    /**
     * 
     * @param likesUrl
     *     The likes_url
     */
    public void setLikesUrl(String likesUrl) {
        this.likesUrl = likesUrl;
    }

    /**
     * 
     * @return
     *     The projectsUrl
     */
    public String getProjectsUrl() {
        return projectsUrl;
    }

    /**
     * 
     * @param projectsUrl
     *     The projects_url
     */
    public void setProjectsUrl(String projectsUrl) {
        this.projectsUrl = projectsUrl;
    }

    /**
     * 
     * @return
     *     The reboundsUrl
     */
    public String getReboundsUrl() {
        return reboundsUrl;
    }

    /**
     * 
     * @param reboundsUrl
     *     The rebounds_url
     */
    public void setReboundsUrl(String reboundsUrl) {
        this.reboundsUrl = reboundsUrl;
    }

    /**
     * 
     * @return
     *     The reboundSourceUrl
     */
    public String getReboundSourceUrl() {
        return reboundSourceUrl;
    }

    /**
     * 
     * @param reboundSourceUrl
     *     The rebound_source_url
     */
    public void setReboundSourceUrl(String reboundSourceUrl) {
        this.reboundSourceUrl = reboundSourceUrl;
    }

    /**
     * 
     * @return
     *     The animated
     */
    public boolean isAnimated() {
        return animated;
    }

    /**
     * 
     * @param animated
     *     The animated
     */
    public void setAnimated(boolean animated) {
        this.animated = animated;
    }

    /**
     * 
     * @return
     *     The tags
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     * 
     * @param tags
     *     The tags
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * 
     * @return
     *     The team
     */
    public Object getTeam() {
        return team;
    }

    /**
     * 
     * @param team
     *     The team
     */
    public void setTeam(Object team) {
        this.team = team;
    }

    @Override
    public int compareTo(Shot another) {

        return this.title.compareToIgnoreCase(another.title);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
        dest.writeParcelable(this.images, flags);
        dest.writeInt(this.viewsCount);
        dest.writeInt(this.likesCount);
        dest.writeInt(this.commentsCount);
        dest.writeInt(this.attachmentsCount);
        dest.writeInt(this.reboundsCount);
        dest.writeInt(this.bucketsCount);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.htmlUrl);
        dest.writeString(this.attachmentsUrl);
        dest.writeString(this.bucketsUrl);
        dest.writeString(this.commentsUrl);
        dest.writeString(this.likesUrl);
        dest.writeString(this.projectsUrl);
        dest.writeString(this.reboundsUrl);
        dest.writeString(this.reboundSourceUrl);
        dest.writeByte(this.animated ? (byte) 1 : (byte) 0);
        dest.writeStringList(this.tags);
        dest.writeSerializable((Serializable) this.user);
        dest.writeParcelable((Parcelable) this.team, flags);
    }

    public Shot() {
    }

    protected Shot(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.width = in.readInt();
        this.height = in.readInt();
        this.images = in.readParcelable(Images.class.getClassLoader());
        this.viewsCount = in.readInt();
        this.likesCount = in.readInt();
        this.commentsCount = in.readInt();
        this.attachmentsCount = in.readInt();
        this.reboundsCount = in.readInt();
        this.bucketsCount = in.readInt();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.htmlUrl = in.readString();
        this.attachmentsUrl = in.readString();
        this.bucketsUrl = in.readString();
        this.commentsUrl = in.readString();
        this.likesUrl = in.readString();
        this.projectsUrl = in.readString();
        this.reboundsUrl = in.readString();
        this.reboundSourceUrl = in.readString();
        this.animated = in.readByte() != 0;
        this.tags = in.createStringArrayList();
        this.user = (User) in.readSerializable();
        this.team = in.readParcelable(Object.class.getClassLoader());
    }

    public static final Parcelable.Creator<Shot> CREATOR = new Parcelable.Creator<Shot>() {
        @Override
        public Shot createFromParcel(Parcel source) {
            return new Shot(source);
        }

        @Override
        public Shot[] newArray(int size) {
            return new Shot[size];
        }
    };
}
