package com.lewitt.pietrogirardi.lewitt.domain.repository;

import com.lewitt.pietrogirardi.lewitt.domain.entities.Shot;

import java.util.List;

public interface ShotRepository extends Repository {
    List<Shot> listShot(int page) throws Exception;
    Shot retrieveShotDetail(Shot Shot) throws Exception;
}
