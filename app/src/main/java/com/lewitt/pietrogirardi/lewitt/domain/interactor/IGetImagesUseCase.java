package com.lewitt.pietrogirardi.lewitt.domain.interactor;

import com.lewitt.pietrogirardi.lewitt.domain.entities.Shot;

import java.util.List;

/**
 * Created by pietrogirardi on 09/07/16.
 */
public interface IGetImagesUseCase extends IUseCase {
    void execute(int page, Callback<List<Shot>> callback);
}
