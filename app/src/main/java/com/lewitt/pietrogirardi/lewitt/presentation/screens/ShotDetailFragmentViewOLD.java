package com.lewitt.pietrogirardi.lewitt.presentation.screens;

import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lewitt.pietrogirardi.lewitt.R;
import com.lewitt.pietrogirardi.lewitt.domain.entities.Shot;

import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class ShotDetailFragmentViewOLD extends Fragment {

    public static final String ARG_SHOT = "shot";
    private View rootView;

    public ShotDetailFragmentViewOLD() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shot_detail, container, false);
        ButterKnife.bind(this, rootView);

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        return rootView;
    }


    @Override
    public void onStart() {
        super.onStart();

        Bundle args = getArguments();
        if (args != null) {
            updateShotDetail((Shot) args.getParcelable(ARG_SHOT));
        }
    }


    public void updateShotDetail(Shot shot) {

        ImageView imgShot = ButterKnife.findById(rootView, R.id.imageDetail);
        CircleImageView imgPlayer = ButterKnife.findById(rootView, R.id.imagePlayer);
        TextView textPlayerName = ButterKnife.findById(rootView, R.id.textNamePlayer);
        TextView textDescription = ButterKnife.findById(rootView, R.id.textDescription);

        Glide.with(getActivity())
                .load(shot.getImages().getNormal())
                .asBitmap()
                //.placeholder(R.drawable.placeholder)
                //.error(R.drawable.imagenotfound)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .fitCenter()
                .centerCrop()
                .into(imgShot);

        Glide.with(getActivity())
                .load(shot.getUser().getAvatarUrl())
                //.placeholder(R.drawable.placeholder)
                //.error(R.drawable.imagenotfound)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .fitCenter()
                .centerCrop()
                .into(imgPlayer);

        textPlayerName.setText(shot.getUser().getName());

        if(shot.getDescription()!=null)
            textDescription.setText(Html.fromHtml(shot.getDescription()).toString());
    }
}
