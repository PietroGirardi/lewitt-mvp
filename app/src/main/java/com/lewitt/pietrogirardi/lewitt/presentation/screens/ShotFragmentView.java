package com.lewitt.pietrogirardi.lewitt.presentation.screens;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

import com.lewitt.pietrogirardi.lewitt.R;
import com.lewitt.pietrogirardi.lewitt.domain.entities.Shot;
import com.lewitt.pietrogirardi.lewitt.presentation.presenter.IShotPresenter;
import com.lewitt.pietrogirardi.lewitt.presentation.presenter.views.IShotView;
import com.lewitt.pietrogirardi.lewitt.presentation.recyclerview.DividerItemDecoration;
import com.lewitt.pietrogirardi.lewitt.presentation.recyclerview.ShotAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pietrogirardi on 10/07/16.
 */
public class ShotFragmentView extends BaseFragment implements IShotView, ShotAdapter.OnItemClickListener {

    public static final int LAYOUT_MANAGER_LIST = 1;
    public static final int LAYOUT_MANAGER_GRID = 2;

    @Inject
    IShotPresenter mShotPresenter;

    @BindView(R.id.main_toolbar)Toolbar toolbar;
    @BindView(R.id.main_collapsing)CollapsingToolbarLayout collapsingToolbarLayout;

    private View mRootView;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressDialog mProgressDialog;

    public static int layoutManagerType;
    private int page = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplicationComponent.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        createAnimation();

        //verifying to don't inflate the layout again when back from detail
        if(mRootView == null) {

            setHasOptionsMenu(true);
            mRootView = inflater.inflate(R.layout.fragment_shot, container, false);
            ButterKnife.bind(this, mRootView);

            mRecyclerView = ButterKnife.findById(mRootView, R.id.rv_list);
            mRecyclerView.setHasFixedSize(true);

            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
            collapsingToolbarLayout.setTitle("leWitt");


            //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), GridLayout.VERTICAL, 2));
            mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            mSwipeRefreshLayout = ButterKnife.findById(mRootView, R.id.swipeRefreshLayout);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // TODO: 7/5/2016 implement update new posts
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });

            mShotPresenter.loadData(page);
            mRecyclerView.addOnScrollListener((RecyclerView.OnScrollListener) mShotPresenter);
        }

        this.setRetainInstance(true);

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mShotPresenter.attachView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mShotPresenter.detachView();
    }

    @Override
    public void showLoading() {
        mProgressDialog = ProgressDialog.show(getActivity(), getString(R.string.dialog_load_title), getString(R.string.dialog_load_message), true);
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showItems(List<Shot> images) {

        //getting info before update list
        float topOffset = 0;
        LinearLayoutManager manager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        int firstItem = manager.findFirstVisibleItemPosition();
        View firstItemView = manager.findViewByPosition(firstItem);
        if(firstItemView !=null)
            topOffset = firstItemView.getTop();

        ShotAdapter adapter = new ShotAdapter(getActivity(), images, null);
        adapter.setRecyclerViewOnClickListenerHack(ShotFragmentView.this);
        mRecyclerView.setAdapter(adapter);

        //setting scroll to last know position
        manager.scrollToPositionWithOffset(firstItem, (int) topOffset);
    }

    @Override
    public void showError(String error) {
        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.dialog_failure_title))
                .setMessage(getString(R.string.dialog_failure_message))
                .setPositiveButton("Close", null)
                .create();
        dialog.show();
    }

    @Override
    public void showEpisodeDetail(Shot shot) {
        ShotDetailFragmentView detailFragmentView = new ShotDetailFragmentView();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ShotDetailFragmentView.ARG_SHOT, shot);
        detailFragmentView.setArguments(bundle);

        createAnimation2(detailFragmentView);

        FragmentTransaction transaction = getActivity().getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, detailFragmentView);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onItemClick(Shot shot) {
        showEpisodeDetail(shot);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_list, menu);
    }

    @Override //MENU
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id){
            case R.id.action_list:
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                //mRecyclerView.setAdapter(shotAdapter);
                layoutManagerType = LAYOUT_MANAGER_LIST;
                break;
            case R.id.action_grid:
                mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                //mRecyclerView.setAdapter(shotAdapter);
                layoutManagerType = LAYOUT_MANAGER_GRID;
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //TRANSITION ANIMATION
    private void createAnimation(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Slide trans1 = new Slide();
            trans1.setDuration(1000);

            Fade trans2 = new Fade();
            trans2.setDuration(1000);

            this.setExitTransition(trans2);
            this.setReenterTransition(trans1);
            //this.setReturnTransition(trans1);
        }
    }

    private void createAnimation2(Fragment fragment){
        //TRANSITION ANIMATION
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Slide trans = new Slide();
            trans.setDuration(1000);

            fragment.setEnterTransition(trans);
        }
    }
}
