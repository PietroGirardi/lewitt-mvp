package com.lewitt.pietrogirardi.lewitt.presentation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.lewitt.pietrogirardi.lewitt.R;
import com.lewitt.pietrogirardi.lewitt.presentation.screens.ShotFragmentView;

/**
 * Created by p.girardi on 7/8/2016.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!getResources().getBoolean(R.bool.isTablet)) {

            if (savedInstanceState != null) {
                return;
            }

            ShotFragmentView shotFragmentView = new ShotFragmentView();

            // Add the fragment to the 'fragment_container' FrameLayout
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, shotFragmentView).commit();
        }

    }
}
