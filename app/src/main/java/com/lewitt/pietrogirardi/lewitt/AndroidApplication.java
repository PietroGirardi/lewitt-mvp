package com.lewitt.pietrogirardi.lewitt;

import android.app.Application;

import com.lewitt.pietrogirardi.lewitt.data.di.ShotDataModel;
import com.lewitt.pietrogirardi.lewitt.presentation.di.ApplicationComponent;
import com.lewitt.pietrogirardi.lewitt.presentation.di.DaggerApplicationComponent;
import com.lewitt.pietrogirardi.lewitt.presentation.di.ShotPresenterModel;

/**
 * Created by pietrogirardi on 10/07/16.
 */
public class AndroidApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();
    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .shotPresenterModel(new ShotPresenterModel(this))
                .shotDataModel(new ShotDataModel())
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
}
