# **Lewitt** #



## **Architecture** ##

![MVP.png](https://bitbucket.org/repo/aMoBR9/images/1234105821-MVP.png)


## **Nexus 5 Screenshots:** ##
![cel1.jpeg](https://bitbucket.org/repo/aMoBR9/images/2459446895-cel1.jpeg) ![cel2.jpeg](https://bitbucket.org/repo/aMoBR9/images/3502880331-cel2.jpeg) ![cel3.jpeg](https://bitbucket.org/repo/aMoBR9/images/649938892-cel3.jpeg) ![cel4.jpeg](https://bitbucket.org/repo/aMoBR9/images/1460568358-cel4.jpeg)

## **Main technologies and resources:** ##
  * **Card View & RecyclerView:** https://developer.android.com/training/material/lists-cards.html
  * **Retrofit:** http://square.github.io/retrofit/
  * **Gson:** https://github.com/google/gson
  * **ButterKnife:** http://jakewharton.github.io/butterknife/
  * **Glide:** https://github.com/bumptech/glide
  * **PeekAndPop:** https://github.com/shalskar/PeekAndPop
  * **CircleImageView:** https://github.com/hdodenhof/CircleImageView